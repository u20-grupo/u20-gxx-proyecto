
// importar mongoose
const mongoose = require('mongoose');

// establecer el scheme del documento
const productoSchema = mongoose.Schema({
    id_prod : Number,
    nombre_prod : String,
    cant_stock_prod : Number,
    categoria_prod : String,
    precio_prod : Number,
    id_proveedor : [mongoose.Types.ObjectId],
    img_prod: String
},
{
    versionKey : false
});

// exportar 
module.exports = mongoose.model('productos', productoSchema);