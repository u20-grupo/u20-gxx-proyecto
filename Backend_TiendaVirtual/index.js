

// Importar el modulo de express
// forma convencional de nodejs
const express = require('express');
// otra forma de JavaScript 
// import express from 'express'

// impotar mongoose para la conexión a mongodb atlas
const mongoose = require('mongoose');

// Imprtar las variables de entorno
require('dotenv').config({path: 'var.env'});

// para establecer las rutas de express
const router = express.Router();

// Importamos la ruta de la configuración con la base de datos
const conectarDB = require('./config/cxn_db');

// para inicializar express
var app = express();
app.use(express.json());


// Importar los CORS
const cors = require('cors');
app.use(cors());

// Solución cors => Tomado de (Configuración de CORS asincrónicamente:):   
// https://www.it-swarm-es.com/es/javascript/como-habilitar-cors-nodejs-con-express/830937099/
var whitelist = ['http://localhost:4000/apirest/', 'http://localhost:4200/apirest/']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { Origin: true } // reflect (enable) the requested Origin in the CORS response
  }else{
    corsOptions = { Origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}


// Establecer la conexión con la base de datos
conectarDB();

// Especificamos que la carpeta "public" es nuestro contenido del Frontend
app.use(express.static('public'));


// -----------------------------------------------------------------------------
// Prueba mínima para saber que funciona el módolo de express
// En realción al entorno web (ejecutar el servidor web)
// app.use('/', function(req, res){
//     res.send("Hola Tripulantes...")
// });
// utilizando una función flecha
// app.use('/', (req, res) => {
//     res.send("Utilizando una función flecha")
// });
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Para utilizar los métodos http =>  GET, POST, PUT, DELETE, etc...
// const router = express.Router();  => agregada en la línea 10 antes de inicializar express()
app.use(router);



// Importar el Controlador de Proveedores => CRUD
const controlProveedor = require('./controllers/controlProveedor');

router.post('/apirest/proveedor/', cors(corsOptionsDelegate), controlProveedor.crear);  // Create
router.get('/apirest/proveedor/', cors(corsOptionsDelegate), controlProveedor.obtener);  // Read
router.get('/apirest/proveedor/:id', cors(corsOptionsDelegate), controlProveedor.obtenerPorId);  // Read
router.put('/apirest/proveedor/:id', cors(corsOptionsDelegate), controlProveedor.actualizar); // Update
router.delete('/apirest/proveedor/:id', cors(corsOptionsDelegate), controlProveedor.eliminar); // Delete

// router.post('/apirest/proveedor/', controlProveedor.crear);  // Create
// router.get('/apirest/proveedor/', controlProveedor.obtener);  // Read
// router.get('/apirest/proveedor/:id', controlProveedor.obtenerPorId);  // Read
// router.put('/apirest/proveedor/:id', controlProveedor.actualizar); // Update
// router.delete('/apirest/proveedor/:id', controlProveedor.eliminar); // Delete


// importar el controlador de productos => CRUD
const controlProducto = require('./controllers/controlProducto');

router.post('/apirest/producto/', cors(corsOptionsDelegate), controlProducto.crear); // Create
router.get('/apirest/producto/', cors(corsOptionsDelegate), controlProducto.obtener); // Read
router.get('/apirest/producto/:id', cors(corsOptionsDelegate), controlProducto.obtenerPorId); // Read
router.put('/apirest/producto/:id', cors(corsOptionsDelegate), controlProducto.actualizar); // Update
router.delete('/apirest/producto/:id', cors(corsOptionsDelegate), controlProducto.eliminar); // Delete

// router.post('/apirest/producto/', controlProducto.crear); // Create
// router.get('/apirest/producto/', controlProducto.obtener); // Read
// router.get('/apirest/producto/:id', controlProducto.obtenerPorId); // Read
// router.put('/apirest/producto/:id', controlProducto.actualizar); // Update
// router.delete('/apirest/producto/:id', controlProducto.eliminar); // Delete




// router.get('/mensaje', function(req, res){
//     res.send('Mensaje con Método GET')

//     // const name_db = 'u20_gxx'
//     // const user = 'u20gxx'
//     // const psw = 'u20gxx'
//     // const uri = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${name_db}?retryWrites=true&w=majority`
//     // const uri = `mongodb+srv://u20gxx:u20gxx@misiontic-uis-jas.krymo.mongodb.net/u20_gxx?retryWrites=true&w=majority`
//     // mongoose.connect(uri)
//     // mongoose.connect(process.env.URI_MONGODB)
//     //     .then(function(){console.log("Base de Datos Conectada")})
//     //     .catch(function(e){console.log("Error: " + e)})

//     // // Establecer la conexión con la base de datos
//     // conectarDB(); 
// });

// router.post('/mensaje', function(req, res){
//     res.send('Mensaje con Método POST')
// });

// router.put('/mensaje', function(req, res){
//     res.send('Mensaje con Método PUT')
// });

// router.delete('/mensaje', function(req, res){
//     res.send('Mensaje con Método DELETE')
// });

// -----------------------------------------------------------------------------

// Asignación de puerto para el servidor web.
// app.listen(4000);
// Utilizando la variable de entorno del puerto
app.listen(process.env.PORT);



// Mensaje para saber que el servidor web está activo
console.log('Servidor Web Ejecutandose desde: http://localhost:4000/');
console.log('"Ctrl + c" para Finalizar el Servidor Web');



// -----------------------------
// // Ingeniero a mi me funciona pero adicional tengo: // Configurar cabeceras y cors
// app.use((req, res, next) => {
// 	res.header('Access-Control-Allow-Origin', '*');
// 	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested, Content-Type, Accept, Access-Control-Allow-Request-Method');
// 	res.header('Access-Control-Allow-Method', 'GET, POST, OPTIONS, PUT, DELETE');
// 	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
// 	next();
// });