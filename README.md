
Command line instructions

You can also upload existing files from your computer using the instructions below.


Git global setup

git config --global user.name "MisiónTIC UIS_JAS" <br>
git config --global user.email "misiontic.formador17@uis.edu.co"


Create a new repository

git clone https://gitlab.com/u20-grupo/u20-gxx-proyecto.git <br>
cd u20-gxx-proyecto <br>
git switch -c main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push -u origin main

Push an existing folder

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/u20-grupo/u20-gxx-proyecto.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push -u origin main <br>

Push an existing Git repository

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/u20-grupo/u20-gxx-proyecto.git <br>
git push -u origin --all <br>
git push -u origin --tags







Líneas agregadas desde el editor vim de la consola...




// Instrucciones Establecidas por React

Inside that directory, you can run several commands:

  npm start <br>
    Starts the development server.

  npm run build <br>
    Bundles the app into static files for production.

  npm test <br>
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd frontend-tiendavirtual <br>
  npm start








