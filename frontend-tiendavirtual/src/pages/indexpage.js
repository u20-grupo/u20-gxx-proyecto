

import { useState } from "react";
import Menu from "../components/menu";

const IndexPage = () => {

    const [productos, setProducto] = useState([]);

    const cargarProductos = () => {
        // fetch('http://localhost:4000/apirest/producto')
        fetch('https://u20gxx-tienda-virtual.herokuapp.com/apirest/producto')
            .then(res => res.json())
            // .then(todosProd => console.log(todosProd));
            .then(todosProd => setProducto(todosProd));
    }

    cargarProductos();

    return (
        <main>
            <h1>
                Productos Tienda Virtual
            </h1>
            <hr />

            <Menu />

            <hr />
            <div className="contProd">

            {productos.map(cadaProd => {
                return (
                    // <div>
                    //     {/* <h3>ObjectId: {cadaProd._id}</h3> */}
                    //     <h3>Nombre: {cadaProd.nombre_prod}</h3>
                    //     <img width="200px" height="200px" alt="Imagen Producto" src={cadaProd.img_prod} />
                    //     <h6>Imagen tomada de google.com</h6>
                    //     <h3>Id: {cadaProd.id_prod}</h3>
                    //     <h3>Cantidad Stock: {cadaProd.cant_stock_prod}</h3>
                    //     <h3>Categoria: {cadaProd.categoria_prod}</h3>
                    //     <h3>Precio: {cadaProd.precio_prod}</h3>
                    //     <h3>Proveedor(es): {cadaProd.id_proveedor}</h3>
                    //     <hr />
                    // </div>

                    <div className="contProd2">
                        <div className="card" style={{width: '18rem'}}>
                            <img height="200px" src={cadaProd.img_prod} className="card-img-top" alt="..." />
                            <div className="card-body">
                            <h5 className="card-title">{cadaProd.nombre_prod}</h5>
                            <hr />
                            <p className="card-text"> <b>Descripción: </b> Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <hr />
                            <p> <b>Id: </b> {cadaProd.id_prod} </p>
                            <hr />
                            <p> <b>Cantidad Stock: </b> {cadaProd.cant_stock_prod}</p>
                            <hr />
                            <p> <b>Categoria: </b> {cadaProd.categoria_prod}</p>
                            <hr />
                            <p> <b>Precio: </b> {cadaProd.precio_prod}</p>
                            <hr />
                            <p> <b>Proveedor(es): </b> {cadaProd.id_proveedor}</p>
                            <hr />
                            <a href="#" className="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>

                );
            })}

            </div>  

        </main>
    );
}

export default IndexPage;