

import Menu from "../components/menu";

const ProveedoresPage = (props) => {

    return (
        
        <main>
            
            <h1>
                Lista de Proveedores
            </h1>
            <hr />

            <Menu />

            <h3>
                Proveedor 1...
            </h3>
            <h2>
                {props.var2}
            </h2>

        </main>
    );
}

export default ProveedoresPage;