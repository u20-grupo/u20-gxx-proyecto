

import { Link } from "react-router-dom";

const Menu = () => {
    return (
        <ul className="nav justify-content-end">
            <li className="nav-item">
                {/* <a className="nav-link active" aria-current="page" href="/">Home</a> */}
                <Link className="nav-link active" aria-current="page" to="/">Home</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/listaProductos">Admin Productos</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/listaProveedores">Admin Proveedores</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link disabled">Login</Link>
            </li>
        </ul>

    );
}


export default Menu;