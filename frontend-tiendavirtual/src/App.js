// import logo from './logo.svg';
import './App.css';
import ProductosPage from './pages/productos';
import ProveedoresPage from './pages/proveedores';
import IndexPage from './pages/indexpage';
// Para seleccionar únicamente la ruta seleccionada
import {Routes, Route} from 'react-router-dom'

// let a = 6;

function App() {
//function App(props) {
  return (
    <div className="App">
      <header>
        <img width="600px" src="https://lms.uis.edu.co/mintic2022/pluginfile.php/1/theme_edumy/headerlogo1/1663168415/MisionTIC-UIS.png" alt="logo" />
        <hr />
      </header>

        <div>
          <Routes>
            <Route path='/' element={<IndexPage />} />
            <Route path='/listaProductos' element={<ProductosPage />} />
            <Route path='/listaProveedores' element={<ProveedoresPage />} />
          </Routes>
          {/* <IndexPage /> */}
          {/* <ProductosPage /> */}
          {/* <ProveedoresPage /> */}
        </div>

        {/* <h1>
          Hola Tripulantes
        </h1>
        <h2>
          {props.variable} + {a}
        </h2> */}

      
    </div>
  );
}

export default App;
